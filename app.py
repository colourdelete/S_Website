from flask import Flask, render_template, abort, Response
from jinja2 import TemplateNotFound
import toml
import os

app = Flask(__name__)

with open('./config.toml', 'r') as file:
    config = toml.load(file)

@app.route('/')
def root():
    return pages('/index.html')

@app.route('/<path:path>')
def pages(path):
    try:
        ext = os.path.splitext(os.path.split(path)[0])[1]
        return Response(render_template(path), mimetype=config['mimetypes'].get(ext, 'text/html'))
    except TemplateNotFound:
        abort(404)

if __name__ == '__main__':
    print('This is ONLY FOR DEVELOPMENT. For production, use a production-ready server.')
    host = config['run'].get('host', None)
    port = config['run'].get('port', None)
    app.run(host = host, port = port)
